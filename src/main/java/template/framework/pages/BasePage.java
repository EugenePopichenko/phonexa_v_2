package template.framework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import template.framework.utils.js.JSUtils;
import template.framework.utils.PropertyLoader;
import template.framework.utils.WebDriverManager;

import java.lang.reflect.Field;
import java.util.List;

public abstract class BasePage {

    protected final WebDriver driver;
    private final String pagePath;
    @FindAll({@FindBy(xpath = "//div[@id = 'cookiesBar']")})
    private List<WebElement> cookiesBar;

    public BasePage(String pagePath) {
        this.driver = WebDriverManager.getDriver();
        this.pagePath = pagePath;
        PageFactory.initElements(driver, this);
    }

    public void open() {
        String pageUrl = PropertyLoader.getProperty("sut.host") + pagePath;
        String currentUrl = driver.getCurrentUrl();

        if (!currentUrl.equals(pageUrl)) {
            if (!currentUrl.startsWith(pageUrl + "?")) {
                driver.navigate().to(pageUrl);
            }
        }

        removeCookieBar(Boolean.parseBoolean(PropertyLoader.getProperty("remove.cookiesBar")));
    }

    public WebElement getField(String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field field = this.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return (WebElement) field.get(this);
    }

    public String getFieldXpath(String fieldName) {
        FindBy annotation;
        try {
            annotation = this.getClass().getDeclaredField(fieldName).getAnnotation(FindBy.class);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
        return annotation.xpath();
    }

    public boolean isElementPresent(String xpath) {
        return driver.findElements(By.xpath(xpath)).size() != 0;
    }

    private void removeCookieBar(boolean removeCookiesBar) {
        if (removeCookiesBar) {
            if (cookiesBar.size() > 0) {
                cookiesBar.forEach(JSUtils::removeElement);
            }
        }
    }
}