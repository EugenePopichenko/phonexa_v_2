package template.framework.pages;


import template.framework.pages.legkovyeavtomobili.LegkovyeAvtomobiliPage;

public class PageBuilder {

    public static IndexPage getIndexPage() {
        return new IndexPage("");
    }

    public static LegkovyeAvtomobiliPage getLegkovyeAvtomobiliPage() {
        return new LegkovyeAvtomobiliPage("/transport/legkovye-avtomobili/");
    }
}