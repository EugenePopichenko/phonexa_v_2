package template.framework.pages;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class IndexPage extends BasePage {

    @FindBy(xpath = "//input[@id='headerSearch']")
    private WebElement searchInputField;

    public IndexPage(String pagePath) {
        super(pagePath);
    }

}
