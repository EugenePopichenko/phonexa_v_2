package template.framework.pages.legkovyeavtomobili;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import template.framework.pages.BasePage;
import template.framework.utils.js.JSUtils;
import template.framework.utils.WebDriverManager;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class FiltersBlock extends BasePage {
    @FindBy(xpath = "//a[@id='choosecat']/parent::div")
    private WebElement categorySelectField;

    @FindBy(xpath = "//li[@id='param_subcat']//span[contains(@class, '3rd-category-choose-label')]")
    private WebElement makeSelectField;
    @FindBy(xpath = "//li[@id='param_subcat']//span[contains(@class, '3rd-category-choose-label')]/parent::a/following-sibling::ul")
    private WebElement makeSuggestDropDown;
    @FindBy(xpath = "//li[@id='param_subcat']//span[contains(@class, 'down')]")
    private WebElement makeSuggestDropDownArrow;

    @FindBy(xpath = "//li[@id='param_model']//span[@class='header block']")
    private WebElement modelSelectField;

    @FindBy(xpath = "//div[contains(@class, 'price')]/a[contains(@class, 'button-from')]/span[contains(@class, 'block')]")
    private WebElement priceFromSelectField;
    @FindBy(xpath = "//div[contains(@class, 'price')]/a[contains(@class, 'button-from')]/following-sibling::label")
    private WebElement priceFromInputField;
    @FindBy(xpath = "//div[contains(@class, 'price')]/a[contains(@class, 'button-from')]/span[contains(@class, 'clear')]")
    private WebElement priceFromClearButton;
    @FindBy(xpath = "//div[contains(@class, 'price')]/a[contains(@class, 'button-to')]/span[contains(@class, 'block')]")
    private WebElement priceToSelectField;
    @FindBy(xpath = "//div[contains(@class, 'price')]/a[contains(@class, 'button-to')]/following-sibling::label/input")
    private WebElement priceToInputField;
    @FindBy(xpath = "//div[contains(@class, 'price')]/a[contains(@class, 'button-to')]/span[contains(@class, 'clear')]")
    private WebElement priceToClearButton;

    @FindBy(xpath = "//li[@id='param_motor_year']//a[contains(@class, 'button-from')]")
    private WebElement yearFromSelectField;
    @FindBy(xpath = "//li[@id='param_motor_year']//a[contains(@class, 'button-to')]")
    private WebElement yearToSelectField;

    @FindBy(xpath = "//li[@id='param_motor_mileage']//a[contains(@class, 'button-from')]")
    private WebElement mileageFromSelectField;
    @FindBy(xpath = "//li[@id='param_motor_mileage']//a[contains(@class, 'button-to')]")
    private WebElement mileageToSelectField;

    @FindBy(xpath = "//li[@id='param_motor_engine_size']//a[contains(@class, 'button-from')]")
    private WebElement engineSizeFromSelectField;
    @FindBy(xpath = "//li[@id='param_motor_engine_size']//a[contains(@class, 'button-to')]")
    private WebElement engineSizeToSelectField;

    @FindBy(xpath = "//li[@id='param_car_body']//span[@class='header block']")
    private WebElement bodyTypeSelectField;

    @FindBy(xpath = "//li[@id='param_fuel_type']//span[@class='header block']")
    private WebElement fuelTypeSelectField;

    @FindBy(xpath = "//li[@id='param_color']//span[@class='header block']")
    private WebElement colorSelectField;

    @FindBy(xpath = "//li[@id='param_transmission_type']//span[@class='header block']")
    private WebElement transmissionTypeSelectField;

    @FindBy(xpath = "//li[@id='param_condition']//span[@class='header block']")
    private WebElement conditionSelectField;

    @FindBy(xpath = "//li[@id='param_car_option']//span[@class='header block']")
    private WebElement carOptionsSelectField;

    @FindBy(xpath = "//li[@id='param_cleared_customs']//span[@class='header block']")
    private WebElement clearedCustomSelectField;

    public FiltersBlock(WebDriver driver) {
        super("");
        PageFactory.initElements(driver, this);
    }

    public WebElement getMakeSuggestDropDown() {
        if (!makeSuggestDropDown.isDisplayed()) {
            JSUtils.scrollIntoView(makeSuggestDropDownArrow)
                    .click();
        }
        return makeSuggestDropDown;
    }

    public List<String> getMakeSuggestDropDownValuesList() {
        return getMakeSuggestDropDownItemsList().stream()
                .map(WebElement::getText)
                .map(str -> (str.contains("\n")) ? str = str.substring(0, str.indexOf("\n")) : str)
                .collect(Collectors.toList());
    }

    private List<WebElement> getMakeSuggestDropDownItemsList() {
        return getMakeSuggestDropDown().findElements(By.xpath("./li"));
    }

    public void setPriceFromValue(String value) {
        JSUtils.scrollIntoView(getPriceFromSelectField());
        if (isElementPresent(
                getFieldXpath("priceFromClearButton"))
        ) {
            JSUtils.scrollIntoView(getPriceFromClearButton()).click();
        }
        getPriceFromSelectField().click();
        new Actions(WebDriverManager.getDriver())
                .moveToElement(getPriceFromInputField())
                .click()
                .sendKeys(value)
                .sendKeys(Keys.ENTER)
                .perform();
        JSUtils.waitForJSExecution();
    }

    public void setPriceToValue(String value) {
        JSUtils.scrollIntoView(getPriceToSelectField());
        if (isElementPresent(
                getFieldXpath("priceToClearButton"))
        ) {
            JSUtils.scrollIntoView(getPriceToClearButton()).click();
        }
        getPriceToSelectField().click();
        new Actions(WebDriverManager.getDriver())
                .moveToElement(getPriceToInputField())
                .click()
                .sendKeys(value)
                .sendKeys(Keys.ENTER)
                .perform();
        JSUtils.waitForJSExecution();
    }
}
