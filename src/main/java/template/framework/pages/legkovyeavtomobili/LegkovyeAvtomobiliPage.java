package template.framework.pages.legkovyeavtomobili;

import lombok.Getter;
import template.framework.pages.BasePage;

@Getter
public class LegkovyeAvtomobiliPage extends BasePage {

    private FiltersBlock filtersBlock = new FiltersBlock(driver);

    public LegkovyeAvtomobiliPage(String pagePath) {
        super(pagePath);
    }
}
