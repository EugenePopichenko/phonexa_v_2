package template.framework.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;


public enum Browsers {
    CHROME {
        public WebDriver getDriver() {
            System.setProperty("webdriver.chrome.driver", PropertyLoader.getProperty("webdriver.chrome.driver"));
            ChromeOptions options = new ChromeOptions();
            options.addArguments("disable-infobars");
            return new ChromeDriver(options);
        }
    },
    FIREFOX {
        public WebDriver getDriver() {
            return new FirefoxDriver();
        }
    };

    public WebDriver getDriver() {
        return null;
    }
}
