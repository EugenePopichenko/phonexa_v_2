package template.framework.utils;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class WebDriverManager {

    private static WebDriver webDriver = null;

    public static WebDriver getDriver() {
        if (webDriver == null) {
            initWebDriver();
        }
        return webDriver;
    }

    public static void initWebDriver() {
        webDriver = Browsers.valueOf(PropertyLoader.getProperty("browser").toUpperCase()).getDriver();
        webDriver.manage().timeouts().implicitlyWait(
                Long.parseLong(PropertyLoader.getProperty("timeouts.implicitlywait")), TimeUnit.SECONDS);
        webDriver.manage().window().setSize(new Dimension(
                Integer.parseInt(PropertyLoader.getProperty("browser.width")),
                Integer.parseInt(PropertyLoader.getProperty("browser.height"))));
    }

    public static WebDriverWait explicitWait() {
        return new WebDriverWait(webDriver, Long.parseLong(PropertyLoader.getProperty("timeouts.explicitlywait")));
    }
}
