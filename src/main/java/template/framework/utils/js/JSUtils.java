package template.framework.utils.js;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import template.framework.utils.WebDriverManager;

public class JSUtils {
    public static WebElement setAttribute(WebElement element, String attrName, String attrValue) {
        JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getDriver();
        js.executeScript(
                "arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attrName, attrValue);
        return element;
    }

    public static void removeElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getDriver();
        js.executeScript(
                "arguments[0].remove();",
                element);
    }

    public static WebElement scrollIntoView(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getDriver();
        js.executeScript(
                "arguments[0].scrollIntoView({behavior: 'auto', block: 'center', inline: 'center'});",
                element);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return element;
    }

    public static void waitForJSExecution() {
        new JSWaiter().waitAllRequest();
    }
}
