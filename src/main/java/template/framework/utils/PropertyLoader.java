package template.framework.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

    private PropertyLoader() {
    }

    /**
     * get property from System properties, which can be specified as f.e: "-Dbrowser=chrome"
     * if not specified in System properties, will be taken from
     * configFile which can be specified in System properties as '-Dconfig.file=/configName.properties'
     * if not specified, properties will be read from "src/main/resources/config.properties"
     *
     * @param property String - property to be read
     * @return String - value of the property
     */
    public static String getProperty(String property) {
        String configFile = System.getProperty("config.file") == null ? "/config.properties" : System.getProperty("config.file");
        return System.getProperty(property) == null ? getConfigProperty(configFile, property) : System.getProperty(property);
    }


    /**
     * @param file String - "'resources' folder relative path in format: 'File.separator' + 'filename.properties'"
     * @param name String - property to be read
     * @return String - value of the property
     */
    private static String getConfigProperty(String file, String name) {
        Properties props = new Properties();
        try {
            props.load(PropertyLoader.class.getResourceAsStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String value = null;

        if (name != null) {
            value = props.getProperty(name);
        }
        return value;
    }
}