package template.framework.utils;

import lombok.extern.log4j.Log4j;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

@Log4j
public class ListenerTest implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        ITestListener.super.onTestStart(result);
        log.info("onTestStart(): " + result.toString());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        ITestListener.super.onTestSuccess(result);
        log.info("onTestSuccess(): " + result.toString());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        ITestListener.super.onTestFailure(result);
        log.error("onTestFailure(): " + result.toString());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        ITestListener.super.onTestSkipped(result);
        log.warn("onTestSkipped(): " + result.toString());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        ITestListener.super.onTestFailedButWithinSuccessPercentage(result);
        log.warn("onTestFailedButWithinSuccessPercentage(): " + result.toString());
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        ITestListener.super.onTestFailedWithTimeout(result);
        log.error("onTestFailedWithTimeout(): " + result.toString());
    }

    @Override
    public void onStart(ITestContext context) {
        ITestListener.super.onStart(context);
        log.info("onStart(): " + context.toString());
    }

    @Override
    public void onFinish(ITestContext context) {
        ITestListener.super.onFinish(context);
        log.info("onFinish(): " + context.toString());
    }
}
