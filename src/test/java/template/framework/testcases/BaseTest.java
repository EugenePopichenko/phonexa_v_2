package template.framework.testcases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.asserts.SoftAssert;
import template.framework.utils.WebDriverManager;

class BaseTest {

    protected SoftAssert softAssert;

    @BeforeClass
    public void setUp() {
        if (WebDriverManager.getDriver().toString().contains("(null)")) {
            WebDriverManager.initWebDriver();
        }
        softAssert = new SoftAssert();
    }

    @AfterMethod
    public void clearCookies() {
        WebDriverManager.getDriver().manage().deleteAllCookies();
    }

    @AfterClass
    public void tearDown() {
        WebDriverManager.getDriver().quit();
    }
}
