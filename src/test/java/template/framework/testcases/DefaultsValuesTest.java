package template.framework.testcases;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import template.framework.pages.PageBuilder;
import template.framework.pages.legkovyeavtomobili.FiltersBlock;
import template.framework.pages.legkovyeavtomobili.LegkovyeAvtomobiliPage;

import java.util.LinkedList;
import java.util.List;


public class DefaultsValuesTest extends BaseTest {

    @DataProvider(name = "validateFilterDefaultsDataProvider")
    public Object[][] validateFilterDefaultsDataProvider() {
        return new Object[][]{
                {"categorySelectField", "Легковые автомобили"},
                {"makeSelectField", "Все"},
                {"priceFromSelectField", "от (грн.)"}, //TODO: the displayed text is "от"
                {"priceToSelectField", "до (грн.)"}, //TODO: the displayed text is "до"
                {"yearFromSelectField", "от"},
                {"yearToSelectField", "до"},
                {"mileageFromSelectField", "от"},
                {"mileageToSelectField", "до"},
                {"engineSizeFromSelectField", "от"},
                {"engineSizeToSelectField", "до"},
                {"bodyTypeSelectField", "Все"},
                {"fuelTypeSelectField", "Все"},
                {"colorSelectField", "Все"},
                {"transmissionTypeSelectField", "Все"},
                {"conditionSelectField", "Все"},
                {"carOptionsSelectField", "Все"},
                {"clearedCustomSelectField", "Все"}
        };
    }

    @Test(dataProvider = "validateFilterDefaultsDataProvider")
    public void validateFilterDefaultsValuesTest(String fieldName, String expectedFieldText) throws NoSuchFieldException, IllegalAccessException {
        LegkovyeAvtomobiliPage legkovyeAvtomobiliPage = PageBuilder.getLegkovyeAvtomobiliPage();
        legkovyeAvtomobiliPage.open();

        FiltersBlock filtersBlock = legkovyeAvtomobiliPage.getFiltersBlock();
        String actualFieldText = filtersBlock.getField(fieldName).getText();

        softAssert.assertEquals(actualFieldText, expectedFieldText);
        softAssert.assertAll();
    }


    @DataProvider(name = "validateMakeListDefaultsValuesDataProvider")
    public Object[][] validateMakeListDefaultsValuesDataProvider() {
        return new Object[][]{{
                    new LinkedList<String>(){{
                    add("Все"); add("Acura"); add("Alfa Romeo"); add("Audi");
                    add("Bentley"); add("BMW"); add("Brilliance"); add("BYD");
                    add("Cadillac"); add("Chana"); add("ChangFeng"); add("Chery"); add("Chevrolet"); add("Chrysler"); add("Citroen");
                    add("Dacia"); add("Dadi"); add("Daewoo"); add("Daihatsu"); add("Dodge");
                    add("Ferrari"); add("Fiat"); add("Ford");
                    add("Geely"); add("GMC"); add("Great Wall");
                    add("Honda"); add("Hummer"); add("Hyundai");
                    add("Infiniti"); add("Isuzu"); add("Iveco");
                    add("JAC"); add("Jaguar"); add("Jeep");
                    add("Kia");
                    add("Lancia"); add("Land Rover"); add("Lexus"); add("Lifan"); add("Lincoln");
                    add("Maserati"); add("Mazda"); add("Mercedes-Benz"); add("MG"); add("MINI"); add("Mitsubishi");
                    add("Nissan");
                    add("Oldsmobile"); add("Opel");
                    add("Peugeot"); add("Pontiac"); add("Porsche");
                    add("Ravon"); add("Renault"); add("Roewe"); add("Rover");
                    add("Saab"); add("Samand"); add("Seat"); add("Skoda"); add("Smart"); add("SsangYong"); add("Subaru"); add("Suzuki");
                    add("Tesla"); add("Toyota");
                    add("Volkswagen"); add("Volvo");
                    add("Wartburg");
                    add("ZX");
                    add("Богдан"); add("ВАЗ"); add("ГАЗ"); add("ЗАЗ"); add("ИЖ"); add("ЛуАЗ"); add("Москвич / АЗЛК"); add("РАФ"); add("УАЗ");
                    add("Другие");
                    }}
        }};
    }

    @Test(dataProvider = "validateMakeListDefaultsValuesDataProvider")
    public void validateMakeListDefaultsValuesTest(List<String> expectedMakeSuggestionsList) {
        LegkovyeAvtomobiliPage legkovyeAvtomobiliPage = PageBuilder.getLegkovyeAvtomobiliPage();
        legkovyeAvtomobiliPage.open();
        FiltersBlock filtersBlock = legkovyeAvtomobiliPage.getFiltersBlock();

        List<String> actualMakeSuggestionsList = filtersBlock.getMakeSuggestDropDownValuesList();

        softAssert.assertEquals(actualMakeSuggestionsList, expectedMakeSuggestionsList);
        softAssert.assertAll();
    }
}
