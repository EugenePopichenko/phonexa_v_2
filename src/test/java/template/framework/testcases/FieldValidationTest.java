package template.framework.testcases;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import template.framework.pages.PageBuilder;
import template.framework.pages.legkovyeavtomobili.FiltersBlock;
import template.framework.pages.legkovyeavtomobili.LegkovyeAvtomobiliPage;

public class FieldValidationTest extends BaseTest {

    @DataProvider(name = "validatePriceFieldValidationPositiveDataProvider")
    public Object[][] validatePriceFieldValidationPositiveDataProvider() {
        return new Object[][]{
                {"0", "Отдам даром", "0"},
                {"1", "1 грн.", "1 грн."},
                {"2", "2 грн.", "2 грн."},
                {"3", "3 грн.", "3 грн."},
                {"4", "4 грн.", "4 грн."},
                {"5", "5 грн.", "5 грн."},
                {"6", "6 грн.", "6 грн."},
                {"7", "7 грн.", "7 грн."},
                {"8", "8 грн.", "8 грн."},
                {"9", "9 грн.", "9 грн."},
                {"2147483647", "2147483647 грн.", "2147483647 грн."}
                //TODO: add more positive data to validate
        };
    }

    @Test(dataProvider = "validatePriceFieldValidationPositiveDataProvider")
    public void validatePriceFieldValidationPositiveTest(String inputVal, String expectedFromVal, String expectedToVal) {
        LegkovyeAvtomobiliPage legkovyeAvtomobiliPage = PageBuilder.getLegkovyeAvtomobiliPage();
        legkovyeAvtomobiliPage.open();
        FiltersBlock filtersBlock = legkovyeAvtomobiliPage.getFiltersBlock();

        filtersBlock.setPriceFromValue(inputVal);
        filtersBlock.setPriceToValue(inputVal);

        softAssert.assertEquals(filtersBlock.getPriceFromSelectField().getText(), expectedFromVal);
        softAssert.assertEquals(filtersBlock.getPriceToSelectField().getText(), expectedToVal);
        softAssert.assertAll();
    }


    @DataProvider(name = "validatePriceFieldValidationNegativeDataProvider")
    public Object[][] validatePriceFieldValidationNegativeDataProvider() {
        return new Object[][]{
                {"u", "от (грн.)", "до (грн.)"},
                {"d", "от (грн.)", "до (грн.)"},
                {"true", "от (грн.)", "до (грн.)"},
                {"&bsp;", "от (грн.)", "до (грн.)"},
                {"/&bsp;", "от (грн.)", "до (грн.)"},
                {"-1", "1 грн.", "1 грн."},
                {"0.1", "01 грн.", "01 грн."}
                //TODO: add more Negative data to validate
                //TODO: clarify AC or rise a Bug
        };
    }

    @Test(dataProvider = "validatePriceFieldValidationNegativeDataProvider")
    public void validatePriceFieldValidationNegativeTest(String inputVal, String expectedFromVal, String expectedToVal) {
        LegkovyeAvtomobiliPage legkovyeAvtomobiliPage = PageBuilder.getLegkovyeAvtomobiliPage();
        legkovyeAvtomobiliPage.open();
        FiltersBlock filtersBlock = legkovyeAvtomobiliPage.getFiltersBlock();

        filtersBlock.setPriceFromValue(inputVal);
        filtersBlock.setPriceToValue(inputVal);

        softAssert.assertEquals(filtersBlock.getPriceFromSelectField().getText(), expectedFromVal);
        softAssert.assertEquals(filtersBlock.getPriceToSelectField().getText(), expectedToVal);
        softAssert.assertAll();
    }

}
