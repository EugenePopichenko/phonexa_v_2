# How to: #
### Run project: ###
"mvn clean test"
### Configure project: ###

List of configurable properties can be found in "src/main/resources/config.properties"

Configuration can be set/changed in "src/main/resources/config.properties"

or in commandline f.e.: "mvn  clean test -Dsut.host=https://auto.ria.com"

* comandline properties override config file properties: if specified in commandline("mvn  clean test -Dsut.host=https://auto.ria.com") and on config.properties(sut.host=https://www.olx.ua), "https://auto.ria.com" will be used for sut.host

# Задание: #

Создать структуру для тестов с разбиением на пакеты и классы и покрыть селениум тестами
страницу https://www.olx.ua/transport/legkovye-avtomobili/dnepr/q-%D0%BB%D0%B5%D0%B3%D0%BA%D0%BE%D0%B2%D1%8B%D0%B5-%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%BE%D0%B1%D0%B8%D0%BB%D0%B8/

### Обязательные кейсы: ###

1) Проверить поля фильтра по умолчанию
2) Проверить список марок автомобилей (что он содержит определенные наименования)
3) Проверить валидацию поля цена (нельзя ввести ничего, кроме цифр)
4) Отфильтровать по полю Пробег выбрав значения из предлагаемого списка и введя вручную
5) Отфильтровать по цене от/до и проверить что все записи на первой странице имеют правильную цену
6) Проверить работу чекбоксов в выпадающем списке "Коробка передач" (значение по умолчанию, когда отмечаешь один из
   вариантов – чекбокс "Все" становится unchecked)

### Условия имплементации: ###

- Использовать Selenium
- Результат работы теста сохранять в логфайл
- При написании тестов использовать Page Object pattern.
- Для сборки и запуска использовать maven
- Использовать implicit и explicit waits (чтобы гарантировать прохождение тестов даже на медленной машине)
- Вычитывать значения для implicit и explicit waits из проперти файла
- Запускать тесты в браузере с шириной окна в 500px
- (опционально) добавить возможность запуска тестов в разных браузерах (firefox, chrome), задавая их имена в проперти
  файле